\chapter{A Robust $A$-stable collocation method for solving a class of stiff initial value problems}

In this chapter, we intend to work out an example of a Runge-Kutta collocation method, explain its derivation and theoretical details of the specific method. Finally, we will carry out numerical experiments to measure its effectiveness with respect to stability and accuracy when solving stiff differential equations.
\section*{Derivation of the family of collocation methods based on Chebyshev-Gauss-Lobatto points}
Our main interest is to get a numerical approximation of the differential equation of the form \eqref{ode} which is stiff.

We start by taking a fixed stepsize, $h$ which means that the value of $t_n$ is equal to $t_0+nh$.

If we take an assumption that at step $t_n$ we have a numerical approximation $y_n\simeq y(t_n)$ to the problem \eqref{ode}, then to obtain the numerical approximation $y_{n+1}$ at time $t_{n+1}$, we shall consider a $(v+1)$-stage Runge-Kutta method defined by the butcher tableau given in \eqref{butcherta} which can be written in the form \eqref{col1} and \eqref{col2}. We note here that this method is stiffly accurate.

\subsection*{Evaluating the collocation points and the coefficients in the Butcher Tableau}
As seen in the other collocation methods we discussed in chapter 4, there is always a procedure followed when getting the collocation points and they are associated with names alluding to historical reasons. Similarly, in our case, the collocation points will be obtained from the Chebyshev-Gauss-Lobatto points given by
\begin{align}
\alpha_j = \cos (\theta_j), \quad \theta_j = \frac{s-j}{s}\pi, \quad j = 0,1,\dots,v,\label{theta}
\end{align}

and therefore the collocation points will be given by
\begin{align}
c_{j+1}=\epsilon_j = \frac{1}{2}(1+\alpha_j). \label{colm}
\end{align}

To get the coefficients $a_{ij}$, we let $z(t)$ be a smooth differentiable function and we define the linear operator associated with the equations \eqref{col1} and \eqref{col2} by
\begin{align}
L_i(z(t),h) = z(t+\epsilon_{i-1}h)-z(t)-h\sum_{j=1}^{v+1}a_{ij}z'(t+\epsilon_{j-1}h).\label{operator}
\end{align}

If we expand $z(t+\epsilon_{i-1}h)$ and $z'(t+\epsilon_{i-1}h)$ using the Taylor series expansion about $t$ and collecting the powers in $h$ we get
\begin{align*}
z(t+\epsilon_{i-1}h) &= z(t) + \epsilon_{i-1}h z'(t)+\frac{(\epsilon_{i-1}h)^2}{2!}z''(t)+\dots+\frac{(\epsilon_{i-1}h)^n}{n!}z^{(n)}(t)+\dots, \\
z'(t+\epsilon_{i-1}h) &= z'(t) + \epsilon_{i-1}h z''(t)+\frac{(\epsilon_{i-1}h)^2}{2!}z^{3}(t)+\dots+\frac{(\epsilon_{i-1}h)^{n-1}}{(n-1)!}z^{(n)}(t)+\dots .
\end{align*}
Substituting the above equations into \eqref{operator} we get
\begin{align*}
L_i(z(t),h)=\sum_{i=1}^\infty\left(\frac{\epsilon_{i-1}^p}{p!}-\frac{1}{(p-1)!}\sum_{j=1}^{v+1}a_{ij}\epsilon_{i-1}^{p-1}\right) h^pz^{(n)}(t)
\end{align*}
and it is from this that we get an equation for evaluating $a_{ij}$ given by
\begin{align}
\sum_{j=1}^{v+1}a_{ij}\epsilon_{i-1}^{p-1} = \frac{\epsilon_{i-1}^p}{p}, \quad j=1,\dots, v+1.\label{system}
\end{align}
From the above algebraic system, we can conclude that the process is a collocation method since it satisfies $C(v)$ given by \eqref{cs}.

We should take a note that the above system have $(v+1)$ uncoupled systems each with $(v+1)$ equations that also have $(v+1)$ number of unknowns. Therefore at the end we will have $(v+1)(v+1)$ coefficients  of $a_{ij}$.

Lets consider one of the coupled system below
\begin{align*}
a_{j1}+a_{j2}+\dots+a_{j(v+1)}&=\epsilon_{j-1},\\
a_{j1}\epsilon_{0}+a_{j2}\epsilon_1+\dots + a_{j(v+1)}\epsilon_v & = \frac{\epsilon^2_{j-1}}{2},\\
\vdots&\\
a_{j1}\epsilon_{0}^v+a_{j2}\epsilon_1^v+\dots + a_{j(v+1)}\epsilon_v^v & = \frac{\epsilon^{v+1}_{j-1}}{2},
\end{align*}
for $j=1,\dots, v+1$.
\vspace*{0.2cm}
\begin{thm}
For $j=1,\dots,v+1$, each system of equations given by
\begin{align*}
A \bold{v_j} = \bold{B_j}
\end{align*} 
where 
\begin{align*}
A = \begin{pmatrix}
1 & 1 & \hdots & 1\\
\epsilon_0 & \epsilon_1 & \hdots & \epsilon_v\\
\epsilon_0^2 & \epsilon_1^2 & \hdots & \epsilon_v^2\\
\vdots & \vdots &\ddots  & \vdots\\
\epsilon_0^v & \epsilon_1^v & \hdots & \epsilon_v^v
\end{pmatrix}, \\
\bold{v_j} = \begin{pmatrix}
a_{j1}\\a_{j2}\\ \vdots \\ a_{j(v+1)}
\end{pmatrix}, \quad 
\bold{B_j} = \begin{pmatrix}
\epsilon_{j-1}\\\frac{\epsilon_{j-1}^2}{2} \\ \vdots \\ \frac{\epsilon_{j-1}^{v+1}}{v+1}
\end{pmatrix},
\end{align*}
has a unique solution.
\end{thm}
\begin{proof}
See \citet{jesus}.
\end{proof}

So far, we have shown the methodology for evaluating the coefficients $c_i$'s and $a_{ij}$'s, we now seek to evaluate the coefficients $b_j$'s.

We recall that this method is stiffly accurate and therefore $b_j = a_{v+1}j$.
\section*{Examples of the methods}
Before we present the examples, we will consider the theorem below without proof.
\vspace*{0.2cm}
\begin{thm}
Let $M(t) = \prod_{i=1}^p(t-c_i)$ and suppose that $M$ is orthogonal to polynomials of degree $(r-1)$, i.e.
\begin{align*}
\int_0^1 M(t)t^{q-1}\mathrm{dt}=0, \quad q=1,\dots, r;
\end{align*}
then the Runge-Kutta collocation method obtained from the $c_i$ has order $(p+r)$.
\end{thm}
From these we present a corollary that will give us the order of the collocation method we have described above if $v$ is even.

\subsection*{Corollary}
When $v$ is even, the Runge-Kutta collocation methods we have described in section 2 will have order $(s+2)$. Proof is presented in \citet{jesus}.

We now construct the Butcher Tableau for different values of $v$. We will manually do the simplest one which is when $v=1$ and afterwards we will present some selected Butcher Tableau for different values of $v$ and evaluate $A$-stability for the respective cases.
\subsection*{When $v=1$}

To get the collocation points we  follow the procedure \eqref{theta} and \eqref{colm}. When $j=0$, we get
\begin{align*}
\theta_0 = \frac{1}{1}\pi = \pi, \quad \alpha_0 = \cos(\pi)=-1,
\end{align*}
and therefore
\begin{align*}
c_1 = \epsilon_0 = \frac{1}{2}(1+(-1)) = 0.
\end{align*}
For $j=1$, the result is presented below
\begin{align*}
c_2 = \epsilon_1 = \frac{1}{2}(1+\cos(0)) = 1.
\end{align*}
We now use the system given by $\eqref{system}$ to evaluate the values of $a_{ij}$. In this case we will have $4$ $a_{ij}$'s. To start with, we evaluate $a_{11}$ and $a_{12}$ as follows
\begin{align*}
a_{11}+a_{12}& = 0,\\
a_{11}\times 0 + a_{12}\times 1 & = 0,
\end{align*}
which when solved simultaneously gives $a_{11} = 0$ and $a_{12}=0$.

With similar arrangement, we solve for $a_{21}$ and $a_{22}$ which both evaluates to $\frac{1}{2}$. Since the method is stiffly accurate then the values of $b_j$ are also equal to $\frac{1}{2}$. Therefore the Butcher tableau is given below

\begin{align}
\begin{tabular}{l|l*{6}{c}r}
$0$ & $0$ & $0$\\
$1$ & $\frac{1}{2}$ & $\frac{1}{2}$ \\
\\ \hline
 & $\frac{1}{2}$ & $\frac{1}{2}$ 
\end{tabular}.
\end{align}
We recall that the Butcher Tableau above is equivalent to the one for the trapezoidal rule and the Lobatto IIIA method of order $2$.

The stability of the above function which is obtained from \eqref{stability} is given by
\begin{align*}
R(z) = \frac{2+z}{2-z}
\end{align*} 
and it is easily seen that on the left half complex plane, $|R(z)|\leq 1$ and therefore the method is $A$-stable.

\subsection*{When $v=2$}
In this case, the Butcher Tableau is given by
\begin{align}
\begin{tabular}{l|l*{6}{c}r}
$0$ & $0$ & $0$ & $0$\\
\\
$\frac{1}{2}$ & $\frac{5}{24}$ & $\frac{1}{3}$ & $\frac{-1}{24}$ \\ 
\\
$1$ & $\frac{1}{6}$ & $\frac{2}{3}$ & $\frac{1}{6}$ \\\hline
 & $\frac{1}{6}$ & $\frac{2}{3}$ & $\frac{1}{6}$
\end{tabular}.
\end{align}
Again this is equivalent to the Lobatto IIIA method. Using the corollary we have defined above, then we can say that this method has order $v+2=4$.

The stability function of this step is given by 
\begin{align*}
R(z)=\frac{12+6z+z^2}{12-6z+z^2}.
\end{align*} 

Therefore, on the left half of the complex plane we can say that this method is $A$-stable since $|R(z)|\leq 1$.

\subsection*{When $v = 3$}
For this case the Butcher Tableau is presented below
\begin{align}
\begin{tabular}{l|l*{6}{c}r}
$0$ & $0$ & $0$ & $0$ & $0$\\
\\
$\frac{1}{4}$ & $\frac{59}{576}$ & $\frac{47}{288}$ & $\frac{-7}{288}$ & $\frac{5}{576}$ \\
\\ 
$\frac{3}{4}$ & $\frac{3}{64}$ & $\frac{15}{32}$ & $\frac{9}{32}$ & $\frac{-3}{64}$ \\ 
\\
$1$ & $\frac{1}{18}$ & $\frac{4}{9}$ & $\frac{4}{9}$ & $\frac{1}{18}$ \\\hline
 & $\frac{1}{18}$ & $\frac{4}{9}$ & $\frac{4}{9}$ & $\frac{1}{18}$
\end{tabular}.
\end{align}
The stability function of this method is given by 
\begin{align*}
R(z) = \frac{384+192z+38z^2+3z^3}{384-192z+38z^2-3z^3},
\end{align*}
and so on the left hand side of the complex plane, $|R(z)|\leq 1$ implying that the method is $A$-stable. Not forgetting that it has order 4.

The Butcher Tableau and the Stability function  for the cases when $v=4$ and $5$ are given in \citet{jesus}.

Therefore we take this opportunity to find the Butcher tableau and stability functions when $v=6$ and $7$.

\subsection*{When $v=6$}
The Butcher Tableau will be given by
\begin{align*}
\begin{tabular}{l|l*{6}{c}r}
$0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$\\
\\
$\frac{2-\sqrt{3}}{4}$ & $\frac{53+9\sqrt{3}}{2520}$ & $\frac{256-47\sqrt{3}}{4032}$ & $\frac{2269-1368\sqrt{3}}{20160}$ & $\frac{164-93\sqrt{3}}{1260}$ & $\frac{2339-1368\sqrt{3}}{20160}$ & $\frac{256-145\sqrt{3}}{4032}$ & $\frac{-17+9\sqrt{3}}{2520}$ \\
\\ 
$\frac{1}{4}$ & $\frac{1}{280}$ & $\frac{189\sqrt{3}+296}{4032}$ & $\frac{233}{2240}$ & $\frac{-23}{1260}$ & $\frac{23}{2240}$ & $\frac{296-189\sqrt{3}}{4032}$ & $\frac{1}{280}$ \\ 
\\
$\frac{1}{2}$ & $\frac{53}{2520}$ & $\frac{16+7\sqrt{3}}{252}$ & $\frac{319}{1260}$ & $\frac{41}{315}$ & $\frac{-31}{1260}$ & $\frac{16-7\sqrt{3}}{252}$ & $\frac{-17}{2520}$ \\
\\
$\frac{3}{4}$ & $\frac{3}{280}$ & $\frac{24+21\sqrt{3}}{448}$ & $\frac{489}{2240}$ & $\frac{39}{140}$ & $\frac{279}{2240}$ & $\frac{24-21\sqrt{3}}{448}$ & $\frac{3}{280}$ \\
\\
$\frac{2+\sqrt{3}}{4}$ & $\frac{53-9\sqrt{3}}{2520}$ & $\frac{256+145\sqrt{3}}{4032}$ & $\frac{2269+1368\sqrt{3}}{20160}$ & $\frac{164+93\sqrt{3}}{1260}$ & $\frac{2339+1368\sqrt{3}}{20160}$ & $\frac{256+47\sqrt{3}}{4032}$ & $\frac{-17-9\sqrt{3}}{2520}$ \\ 
\\
$1$ & $\frac{1}{70}$ & $\frac{8}{63}$ & $\frac{8}{35}$ & $\frac{82}{315}$ & $\frac{8}{35}$ & $\frac{8}{63}$ & $\frac{1}{70}$\\\hline
 & $\frac{1}{70}$ & $\frac{8}{63}$ & $\frac{8}{35}$ & $\frac{82}{315}$ & $\frac{8}{35}$ & $\frac{8}{63}$ & $\frac{1}{70}$
\end{tabular},
\end{align*}
and the stability function is given by 
\begin{align*}
R(z) = \frac{2580480+1290240z+291840z^2 +38400z^3+3108z^4+146z^5+3z^6}{2580480-1290240z+291840z^2 -38400z^3+3108z^4-146z^5+3z^6}.
\end{align*}
In the left half complex plane, $|R(z)|\leq 1$ and therefore this method is $A$-stable.

We warn that putting the system of linear equations in a numerical software might give us decimal places instead of fractions as we have above and so we recommend the use of the equation \eqref{aij} to evaluate the coefficients $a_{ij}$'s for this case and other cases where $v$ is large. The method has order 8 as defined by the lemma we mention earlier in this section.

\subsection*{When $v= 7$} This is a complex case where the values of $a_{ij}$ will be represented in 4 significant figures. We present this method to demonstrate the complexity of the process as $v$ increases.
\begin{align*}
\resizebox{16cm}{!}{
\centering
\begin{tabular}{l|l*{6}{c}r}
$0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$ & $0$\\
$0.04952$ & $0.02010$ & $0.03198$ & $-0.003601$ & $0.001621$ & $-0.001007$ & $0.0007512$ & $-0.0006388$ & $0.0003032$ \\ 
$0.1883$ & $0.001961$ & $0.1164$ & $0.07893$ & $-0.01331$ & $0.007113$ & $-0.005011$ & $0.004160$ & $-0.001961$ \\ 
$0.3887$ & $0.01657$ & $0.08068$ & $0.1986$ & $0.1054$ & $-0.01882$ & $0.01081$ & $-0.008310$ & $0.003837$ \\
$0.6113$ & $0.006367$ & $0.1034$ & $0.1653$ & $0.2374$ & $0.1132$ & $-0.02244$ & $0.01439$ & $-0.006367$ \\
$0.8117$ & $0.01217$ & $0.09091$ & $0.1811$ & $0.2115$ & $0.2319$ & $0.09719$ & $-0.02130$ & $0.008243$ \\ 
$0.9505$ & $0.009901$ & $0.09571$ & $0.1754$ & $0.2196$ & $0.2170$ & $0.1797$ & $0.06309$ & $-0.009901$\\
$1$ & $0.01020$ & $0.09507$ & $0.1761$ & $0.2186$ & $0.2186$ & $0.1761$ & $0.09507$ & $0.01020$\\\hline
 & $0.01020$ & $0.09507$ & $0.1761$ & $0.2186$ & $0.2186$ & $0.1761$ & $0.09507$ & $0.01020$
\end{tabular}.
}
\end{align*}
We obtain the following as the stability function
\begin{align*}
\begin{split}
\frac{4.238\times 10^{-8}z^7+2.797\times 10^{-6}z^6+8.1380\times 10^{-5}z^5+0.001409z^4+0.01581z^3+0.0049z^2+0.5z+1}{-4.238\times 10^{-8}z^7+2.797\times 10^{-6}z^6-8.1380\times 10^{-5}z^5+0.001409z^4-0.01581z^3+0.0049z^2-0.5z+1}.
\end{split}
\end{align*}
Note that in the above example, we have rounded everything to $4$ significant figures. The method above is also $A$-stable and has order 8.

In the above examples that we have presented and those that are in \citet{jesus}, we notice that the method is not $L$-stable since 
\begin{align*}
\lim_{z\to\infty} R(z) = \pm 1.
\end{align*}

%This method here has order 8.

\section*{Characteristics of the methods}
The first characteristics we observe is that the stage order of the internal stages of this method is atleast $\mathcal{O}(h^{s+2})$.

%The second characteristics that can be seen is that since the first row of the matrix $A$ just consists of zeros, then it means that the first stage coincides with the initial values. Again since they are stiffly accurate, then the last stage coincides with the final values and so no further calculation is needed to evaluate $y_{n+1}$.

We have observed that the first stage of this methods coincides with the initial values while the last stage coincides with the final values. This implies that no further calculation is needed to find the value of $y_{n+1}$.

Another characteristics that we have stressed throughout the constructions of the Butcher tableau is that the method is $A$-stable.

A careful observation of the collocation points lead us to the following conclusion
\begin{align*}
c_i + c_{v+2-i}=1, \quad i = 1,\dots, v+1,
\end{align*}

Aand therefore we conclude that the numerical approximation after one step backward will lead us to the initial value, that is, applying the step-size $-h$ to $y_{n+1}$ will lead us to $y_n$.

The stability function of this method takes the form
\begin{align*}
R_v(z)=\frac{P_v(z)}{P_v(-z)},
\end{align*}
where $P_v(z)$ is a polynomial of degree $v$ that is obtained by 
\begin{align*}
P_v(z) = M^{v+1}(1)+M^{v}(1)z+\dots + M'(1)z^v,
\end{align*}
and
\begin{align*}
M(z) = \prod_{i=0}^v (z-\epsilon_i),
\end{align*}
with $\epsilon_i$ is defined by the equation \eqref{colm}.

\section*{Connection between Runge-Kutta Collocation method and linear multistep methods}

A numerical method is said to be an $l$ step method if $y_{n+1}$ depends on $y_{n+1-l},\dots,y_n$. Linear multistep methods are described by the relation below
\begin{align}
\sum_{j=0}^l \alpha_j y_{n+j}=h_n \sum_{j=0}^l \beta_j F(t_{n+j},y_{n+j}),n=0,\dots,N-l,\label{multi}
\end{align}

where $\alpha$ and $\beta$ are constants to be determined.

Linear multisteps methods are divided into many classes and we will mention a few that we will encounter in this section.
\begin{enumerate}
\item \textbf{The Adams method.} In these methods, the values of $\alpha$ on  the left hand side of the equal sign of equation \eqref{multi} are given by 
\begin{align*}
\alpha_0=\alpha_1=\dots=\alpha_{l-2}=0,\alpha_{l-1}=-1,\alpha_l=1.
\end{align*}
These methods are given by the following expression
\begin{align*}
y_{n+l}=y_{n+l-1}+h_n \sum_{j=0}^l \beta_j F(t_{n+j},y_{n+j}).
\end{align*}
If the value of $\beta_l=0$ then this method is explicit and it has a special name, {\em Adams-Bashforth methods} otherwise if $\beta_l\neq 0$ we get the {\em Adams-Moulton methods} which are implicit.
\item \textbf{Milne-Simpson Methods.} These methods take the form 
\begin{align*}
y_{n+l}=y_{n+l-2}+h_n \sum_{j=0}^l \beta_j F(t_{n+j},y_{n+j}).
\end{align*}
\end{enumerate}

Computations for the vales of $\beta_j$ in the multistep problems indicated above can be found in \citet{hawa}.

A careful look at our collocation method and the linear multistep methods shows that their is a close link between them.

We start with the case when $v=1$ and  as we had mentioned before, the Runge-Kutta collocation method is equivalent to the trapezoidal rule which is a linear multistep method.

Next, we observe the case when $v=2$,  and we can use the Butcher tableau to obtain the following system of equations

\begin{align}
y_{n+\frac{1}{2}} &= y_n + \frac{h}{24}\left(5 f_n + 8f_{n+\frac{1}{2}}-f_{n+1}\right), \\
y_{n+1} &= y_n + \frac{h}{6}\left(f_n + 4f_{n+\frac{1}{2}}+f_{n+1}\right). \label{sec}
\end{align}

It is easily observed that the first equation corresponds to the two-steps Adams Moulton formula for the equation of $y' = f(x)$ given by
\begin{align*}
y_{n+1} = y_n + \bar{h}\left(\frac{5}{12}f_{n+1}+\frac{8}{12}f_n - \frac{1}{12}f_{n-1}\right),
\end{align*}
and to get the equality between the two equations then we realize that if we take $x_{n-1}=t_n+h$, $x_n = t_n +\frac{h}{2}$, $x_{n+1}=t_n$ and $\bar{h}=\frac{-h}{2}$.

By setting $x_{n-1}=t_n$, $x_n = t_n+\frac{h}{2}$, $x_{n+1}=t_n+h$ and $\bar{h} = \frac{h}{2}$, then we see that the two-step Milne Simpson formula for the equation $y' = f(x,y)$ given by
\begin{align*}
y_{n+1} = y_{n-1}+\bar{h}\left(\frac{1}{3}f_{n+1}+\frac{4}{3}f_n+\frac{1}{3}f_{n-1}\right),
\end{align*} 
will lead us to the equation \eqref{sec}.

For cases when $v\geq 3$, then the computations become complicated but the method for solving them is just the same, so we will demonstrate the procedure for solving when $v=3$ and the rest of the cases can be approached the same way.

From the Butcher tableau corresponding to $v=3$, we get the following systems
\begin{align*}
y_{n+\frac{1}{4}} &= y_n +\frac{h}{576}\left(59f_n + 94 f_{n+\frac{1}{4}}-14f_{n+\frac{3}{4}}+5f_{n+1}\right),\\
y_{n+\frac{3}{4}} &= y_n +\frac{3h}{64}\left(f_n + 10 f_{n+\frac{1}{4}}+6f_{n+\frac{3}{4}}-f_{n+1}\right),\\
y_{n+1} &= y_n +\frac{h}{18}\left(f_n + 8f_{n+\frac{1}{4}}+8f_{n+\frac{3}{4}}+f_{n+1}\right).
\end{align*}

The three equations above lead us to the three-step Adams-Bashforth formula, three-step Milne-Simpson formula and the variable-step Milne-Simpson formula of three steps respectively if we take the following adjustments $x_{n-2}=t_n+h$, $x_{n-1}=t_n+\frac{3h}{4}$, $x_n=t_n+\frac{h}{4}$ and $x_{n+1}=t_n$.

\section*{Implementation Strategy}
Applying this collocation method to large systems of ODEs causes large systems of equations to be solved in each step. If we have $m$ systems of differential equations then the dimensions of the system will be $(v+1)m\times (v+1)m$.

We can rewrite \eqref{col1}-\eqref{col2} as 
\begin{align*}
y_{n+c_1} &= y_n\\
y_{n+c_2} &= y_n+h_n\sum_{j=1}^v a_{2j}f(t_n+c_j h_n,Y_{n,j})\\
\vdots \\
y_{n+1} &=y_n+h_n\sum_{j=1}^v a_{(v+1)j}f(t_n+c_j h_n,Y_{n,j}).
\end{align*}

We introduce the notations
\begin{align*}
Y_n &= (y_{n+c_1},\dots,y_{n+c_{v+1}})^T\in \mathbb{R}^{(v+1)m},\\
F(t_n,Y_n) &= (f(t+c_1h_n,y_{n+c_1}),\dots,f(t+c_{v+1}h_n,y_{n+c_{v+1}}))^T\in \mathbb{R}^{(v+1)m},\\
{e}&=(1,\dots,1)^T\in \mathbb{R}^n.
\end{align*}
with $n$ being the dimension of our ODE.

To write our systems of ODE we use the kronecker product defined below
\vspace*{0.2cm}
\begin{defn}
Let $A\in R^{(v+1),m}$ and $B\in \mathbb{R}^{p,q}$, we define the kronecker product $A\otimes B$ as the $((v+1)p,mq)$ matrix shown below
\begin{align*}
A\otimes B = \begin{pmatrix}
a_{11}B&\hdots,& a_{1m}B\\
\vdots & \ddots & \vdots\\
a_{(v+1)1}B & \hdots & a_{(v+1)m}B
\end{pmatrix}.
\end{align*}
\end{defn}

Using this definition in writing down the systems of ODE we get
\begin{align*}
{e}\otimes \mathbf{y}_n & = (\mathbf{y}_n,\dots,\mathbf{y}_n)^T\in \mathbb{R}^{(v+1)m},\\
\mathbf{A}\otimes {I}_n & = \begin{pmatrix}
0 & \dots & 0\\ 
a_{21} I_n&\hdots,& a_{2m}I_n\\
\vdots & \ddots & \vdots\\
a_{(v+1)1}I_n & \hdots & a_{(v+1)m}I_n
\end{pmatrix},\\
b^T\otimes I_n &= (b_1 I_n,\dots, b_{v+1}I_n)^T\in \mathbf{R}^{m,m(v+1)}
\end{align*}

where $I_n\in \mathbb{R}^{n,n}$ denotes the identity matrix.

We can therefore rewrite the Runge-Kutta collocation method \eqref{col1}-\eqref{col2} in a more compact form as
\begin{align*}
Y_n={e}\otimes \mathbf{y}_n+h_n(A\otimes I_n)\mathbf{F}(t_n,Yn).
\end{align*}

We therefore the following system to find our approximation
\begin{align*}
\mathbf{G}(Y_n)=Y_n-{e}\otimes \mathbf{y}_n-h_n(A\otimes I_m)F(t_n,Y_n).
\end{align*}

Note that in this project we have used a uniform step-size in our numerical simulations.


%\subsection*{Strategy for step-size selection}
%Most of the Runge-Kutta methods usually use an embedded pair of methods for step-size selection but in our case that will be costly. We will therefore consider another approach for getting the local truncation error when $v$ is even.
%
%We start by taking into account that $\epsilon_{\frac{s}{2}}=\frac{1}{2}$ in the method that we have designed in this chapter and consider the linear step of order 2 given by
%\begin{align*}
%hf_{n+1} = 3y_{n+1}+y_n-4y_{n+\frac{1}{2}}.
%\end{align*}
%From the given equation, if we take $z(t)$ as the solution of \eqref{ode}, we have
%\begin{align}
%hf(t_{n+1},z(t_{n+1})) = 3z(t_{n+1})+z(t_n)-4z(t_{n+\frac{1}{2}})+O(h^3).\label{loca}
%\end{align}
%By assuming that $y_{n+\frac{1}{2}}$ is the approximate solution of the intermediate stage obtained in our method and also assuming the localization hypothesis we have
%\begin{align*}
%y_n = z(t_n),\quad y_{n+\frac{1}{2}}=z\left(t_{n+\frac{1}{2}}\right).
%\end{align*} 
%Expanding $f_n$ on the left hand side of \eqref{loca} by Taylor's approximation, we obtain
%\begin{align*}
%\left(3 I_m - h\frac{\delta f}{\delta y}\left(t_{n+1},y_{n+1}\right)\right)\left(z(t_{n+1})-y_{n+1}\right)=hf(t_{n+1},y_{n+1})-y_n+4y_{n+\frac{1}{2}}-3y_{n+1}+O(h^3),
%\end{align*}
%where $\frac{\delta f}{\delta y}$ denotes the Jacobian matrix and $I_m$ is the identity matrix of order $m$.
%
%The estimator for the local truncation error is obtained from the relation below
%\begin{align*}
%||z(t_{n+1})-y_{n+1}||=\text{err} + O(h^3),
%\end{align*}
%where 
%\begin{align*}
%\text{err} = ||M^{-1}(hf(t_{n+1},y_{n+1})-y_n+4y_{n+\frac{1}{2}}-3y_{n+1})||,
%\end{align*}
%and $M$ is the matrix
%\begin{align*}
%M=3 I_m - h\frac{\delta f}{\delta y}(t_{n+1}-y_{n+1}).
%\end{align*}
%
%We notice that the only extra cost of employing this strategy is the one for changing the step-size which will only be in one function per step but since this value is going to be used in the following step, we realize that there is no extra cost after all. 
%
%Having the local error, the standard step-size prediction leads to
%\begin{align*}
%h_{\text{new}}=\tau h_{\text{old}}\left(\frac{\text{atol}}{||\text{err}||}\right)^{\frac{1}{3}},
%\end{align*}
%for a given tolerance, atol, where $\tau$ is a safety factor.
%
%To minimize excessive computations, we consider the following strategy
%\begin{align*}
%k_1 h_{\text{old}}\leq h_{\text{new}}\leq k_2 h_{\text{old}},
%\end{align*}
%with, say, $k_1=1.0 $ and $k_2=1.5$.
