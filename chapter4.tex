\chapter{Runge-Kutta collocation methods}

Before we dive into collocation methods it is necessary to review the topics on polynomial interpolation and Numerical quadrature.
\section*{Polynomial interpolation}
If we have $\mathbb{P}_v$ as the space of real polynomials of degree $\leq v$, such that we have a set of $v$  distinct interpolation  points  $c_1<\dots <c_v$, $c_i\in \mathbb{R}$ and a set of their corresponding data given by $g_1,\dots, g_v$, then the interpolating polynomial is the unique  polynomial $P(x)\in \mathbb{P}_v$ satisfying $P(c_i)=g_i$, $i=1,\dots, v$.

One popular interpolating polynomial that we will use in this project is the Lagrange interpolating polynomial given by
\begin{align}
\kern 1pt \ell_j(x) =\prod_{k\neq j}^v\frac{x - c_k}{c_j-c_k}.\label{lagran}
\end{align}

The basis for $\mathbb{P}_v$ are formed by this interpolating polynomials and they assume the following equations
\begin{align}
P(x)=\sum_{j=1}^v g_j \kern 1pt \ell_j(x). \label{lagra}
\end{align}

\section*{Numerical quadrature}
Given a smooth function $g\in \mathbb{R}$, powerful numerical  methods have been developed to approximate the integral of $g$ on the interval $[0,1]$. Finding the approximate integral involves integrating the interpolating polynomial of order $v-1$ from $v$ quadrature points $0\leq c_1<\dots<c_v\leq 1$ where $g$ are the values at the quadrature points $g_j=g(c_j)$, $j=1,\dots,v$.

If we define the weights 
\begin{align}
b_j = \int_0^{1} \kern 1pt \ell_j(\tau) \mathrm{d\tau},
\end{align}
then we get the quadrature formula as
\begin{align}
\int_0^1 g(x)\approx \int_0^1 P(x)=\sum_{j=1}^v b_j g(c_j).
\end{align}

Now, to get the integral $\int_{t_0}^{t_0+h}g(t)\mathrm{dt}$, we repeat the process defined by  \eqref{transform}-\eqref{finis}.

\section*{Collocation methods}
We adopt the definition of collocation methods from \citet{hermann}.
\vspace*{0.2cm}
\begin{defn}
A collocation solution $u_h$ to a functional equation (for example an ordinary differential equation or a Volterra integral equation) on an interval $I$ is an element from some finite-dimensional function space (the collocation space) which satisfies the equation on an appropriate finite subset of points in $I$ (the collocation points) whose cardinality essentially matches the dimension of the collocation space. If initial conditions are present then $u_h$ will usually be required to fulfil these conditions, too.
\end{defn}

Therefore to find the solution of the ordinary differential equation \eqref{diff1} by a collocation method over the interval $[t_0,t_0+h]$, we choose collocation points $0\leq c_1<c_2<\dots <c_n\leq 1$ and then find a collocation polynomial of degree $\leq n$, which satisfies
\begin{align*}
y(t_0)&=y_0,\\
y'(t_0+c_i h)&=f(t_0+c_i h,y(t_0+c_i h)), \quad i=1,\dots ,s.
\end{align*} 
and its numerical solution is defined by $y_1=y(t_0+h)$.


%All the collocation methods are in fact implicit Runge-Kutta methods in disguise.

The reason why collocation methods are considered a powerful tool in solving  a system of ordinary differential  equations is that it enables us to not only get a discrete set of solutions but also a continuous approximation to the solution. 

%For convenience, we will introduce the local Lagrange basis representations of $u_n$ which leads us to the following equation

The interpolation polynomial $y'(t_0+c_i h)$ can be found by the Lagrange interpolation as shown in \eqref{lagra}. Therefore the interpolation polynomial becomes
\begin{align}
y'_n(t_n+\tau h_n) =\sum_{j=1}^v \kern 1pt \ell_j(\tau)  Y_{n,j}; \quad Y_{n,j}=y'_n(t_n+c_j h_n),\label{lagr}
\end{align}
where $\kern 1pt \ell_j$ are the Lagrange polynomial corresponding to the collocation points $c_i$ given by \eqref{lagran}.


If we integrate equation \eqref{lagr} we have
\begin{align*}
y_n(t_n+c_i h_n) =y_0 + h\sum_{j=1}^v y'_n(t_n+c_j h_n)\int_0^{c_i} \kern 1pt \ell_j(\tau) \mathrm{d\tau}, 
\end{align*}
and if $c_i = 1$, we get
\begin{align*}
y_n(t_n+ h) =y_0 + h\sum_{j=1}^v y'_n(t_n+ h) \int_0^{1} \kern 1pt \ell_j(\tau) \mathrm{d\tau}=y_0 + h\sum_{j=1}^v b_j y'_n(t_n+ h). 
\end{align*}


If we take 
\begin{align}
a_{ij}=\int_0^{c_i} \kern 1pt \ell_j(\tau) \mathrm{d\tau}, \label{aij}
\end{align} 
then we get the equation \eqref{col1} and equation \eqref{col2}.

We note that $A=(a_{ij})_{i,j = 1}^v$, the elements of the coefficient matrix, $b=(b_1,\dots,b_v)^T$, the weight vector and $c=(c_1,\dots,c_v)^T$ the node vector are provided in the Butcher tableau.

The coefficients of a collocation method must satisfy the following properties
\begin{align}
C(v): A\cdot c^{q-1}=\frac{1}{q}c^{q},
\quad 1\leq q \leq v \label{cs},\\
B(v): \sum_{i=1}^s b_i c_i^{q-1}, \quad 1\leq q\leq v. \label{bs}
\end{align} 
Most of the collocation methods usually use collocation points obtained from the normalized Legendre Polynomials of degree $s$ given by the following equation
\begin{align}
P_k^*(x)=\sum_{j=0}^k(-1)^{j+k}\dbinom{k}{j}\dbinom{k+j}{j}x^j, \quad k=0,1,\dots ,s. \label{legendre}
\end{align}
%or
%\begin{align*}
%P_v(2x-1)=\frac{1}{v !}\frac{\mathrm{d^s}}{\mathrm{d t}}[t^s(t-1)^s]
%\end{align*}
Some of the special cases of these collocation methods obtained from these polynomials are

%the Radau methods where for Radau I method, $c_1 = 0$ and for the Radau II method, $c_v = 1$. The Radau method have order $2v-2$. Another special case are the Lobatto methods where $c_0=0$ and $c_v=1$. The Lobatto methods have order $2v-3$.

\begin{itemize}
\item Methods based on Gauss points. The collocation points, $c_i$, are the roots of \eqref{legendre}. The Gauss formula are exact upto order $2v-1$.


\item Methods based on Radau points. These ones are divided into two. 

The first of these is the Radau I method. In this method, the collocation points are the roots of the polynomial $P_v^*(x)+P_{v-1}^*(x)$. After evaluating the roots, we will get $c_1=0$.

They are exact upto degree $2v-2$.

The second case of these methods is the Radau II methods where the collocation points are the roots of the polynomial $P_v^*(x) - P_{v-1}^*(x)$. Evaluating the roots yields $c_v=1$. 

The Radau II formula is exact upto polyn degree $2v-2$.

A specical case of the Radau II methods, Radau  IIA, plays a very important role in the integration of stiff differential equations.

\item Methods based on Lobatto points. In these methods, the collocation are the roots of the polynomial $P_v^*(x)-P_{v-2}^*(x)$. Evaluation of the roots will result to $c_0=0$ and $c_v=1$.

They are stiffly accurate with an explicit first stage. Therefore the first stage do not require any computations because they coincide with the last stage of the previous step.

The Lobatto formula is  exact upto degree $2v-3$ and therefore have order $2v-2$. A special case of the Lobatto methods, Lobatto IIIA methods have the highest possible order.
\item Strongly $A$-stable First Explicit Runge-Kutta (SAFERK) methods. The SAFERK methods are a recently introduced method for solving stiff differential equations. They aim at achieving stability of the process.

The collocation points of the SAFERK methods are obtained from the equation below
\begin{align*}
\sqrt{2 v+1}(P^*_v(x)-P_{v-2}^*(x))+\alpha\sqrt{2v-1}(P_{v-1}^*(x)-P_{v-3}^*(x)),
\end{align*}
where $\alpha$ is a free parameter chosen to achieve $A$-stability.

These methods are exact upto degree $2v-4$ implying that they have a classical order of $2v-3$.

The $v$-stage of this method is equivalent to the $(v-1)$-stage RADAU IIA method since they have the same number of implicit steps with the exception being that the SAFERK methods have an initial explicit stage.

They are also stiffly accurate and strongly $A$-stable. It is easily observed that if we take $\alpha = 0$, then we obtain the Lobatto IIIA method.

The $A$-matrix of the SAFERK method will take the form
\[
A =
\left( \begin{array}{c|c}
   0 &  0^T\\\hline
   A_1 &  \widetilde{A}
\end{array} \right).
\]
where $\widetilde{A}$ is a $(v-1)\times (v-1)$ non singular matrix and $A_1$ a column vector of dimension $1$.
\end{itemize}

The derivation and properties of the first three methods can be found in \citet{butcher} while the SAFERK method is found in \citet{pinto} and the references therein.


We also have collocation methods that use the Chebyshev-Gauss-Lobatto points which will be part of our discussion in chapter 6.

The reason the collocation methods are usually used in approximating stiff differential equations is because they have excellent stability properties.


A numerical method is stable for a stepsize and a formula selection scheme if small perturbations in the initial condition do not cause the numerical approximation to diverge away from the true solution given that the true solution is bounded.



We therefore investigate the stability properties of the collocation methods in the next section.

\section*{Stability analysis for Runge-Kutta methods}
% Given a differential equation of the form $y'(t)=\lambda y(t)$, then we denote the stability function to be the ratio of the previous numerical solution to the one we have, that is, $y_n/y_{n-1}$ and is denoted by $R(z)$.
 
When considering numerical methods for initial value problems, the concept of absolute stability is very important. To introduce this concept, we consider the test equation 
\begin{align}
y'(t)=\lambda y(t)\label{dahl}.
\end{align}

This equation might appear to be very simple but it is very important in predicting the stability behaviour of  given numerical methods.

If we apply the numerical methods to the test equation \eqref{dahl} we get a first order difference equation
\begin{align*}
y_{n+1}=R(z)y_n,
\end{align*}

where $z=\lambda h$. 

We refer to the function $R(z)$ as the {\em stability function} of the method. We therefore define the stability domain of the method below that can be found in \citet{butcher}.

 \vspace*{0.2cm}
 \begin{defn}
 The stability region, $\{z\in \mathbb{C}:|R(z)|<1\}$, is the set of points that allows the solution to remain bounded even after many steps of calculation. We concentrate in the negative half plane because the solution is bounded and would therefore allow a good modelling of the differential equation.
 \end{defn}
 
 \vspace*{0.2cm}
 \begin{defn}\citet{jose}.
 If $|R(z)|\leq 1$ for all $z$, then we say that the method  is absolutely stable.
 \end{defn}
 
\subsection*{A-Stability}
A numerical method is considered useful if it preserves stability. This numerical method will usually perform better within their stability domains but will give less accurate results outside this domains. Therefore a method that have large domains will always perform better than those that have smaller domains.

\vspace*{0.2cm}
\begin{defn}\citet{wanner} defines
a method, whose stability domain satisfies
\begin{align*}
S\supset \mathbb{C}^- = \{z; \mathrm{Re} z\leq 0\},
\end{align*}
as $A$-stable.
\end{defn}

\vspace*{0.2cm}
\begin{defn}
According to \citet{wanner}, a method is called $L$-stable if it is $A$-stable and if in addition
\begin{align*}
\lim_{z\to\infty} R(z)=0.
\end{align*}
\end{defn}

%Another important definition we probably need to know is  Stiff stability
%
%\vspace*{0.2cm}
%\begin{defn}
%A method is stiffly stable if there is a D such that its stable around $Re(z)<D$ and it is A-stable.
%\end{defn}

%Since the collocation methods are implicit Runge-Kutta methods, to find their stability functions, we start with the implicit Euler method
%\begin{align*}
%y_1 = y_0 + hf(t_1,y_1).
%\end{align*}
%Applying this to $y'=\lambda y$, the equation becomes
%\begin{align*}
%y_1 = y_0 + h\lambda y_1,
%\end{align*}
%which leads to 
%\begin{align*}
%y_1=\frac{1}{1-h\lambda}y_0 = R(h\lambda)y_0.
%\end{align*}
%Taking $z=h\lambda$ we get 
%\begin{align*}
%y_1 = R(z)y_0.
%\end{align*}
%We refer to $R(z)$ as the stability function of the Euler implicit method.

To find the stability of Runge-Kutta collocation method, we follow the procedure below.

The $s$-stage implicit Runge-Kutta methods given by \eqref{col1} and \eqref{col2} applied to $y'=\lambda y$ will yield
\begin{align*}
\bold{Y} = \bold{y_0} + h\lambda A \bold{Y}\quad\Rightarrow \bold{Y} = (1-zA)^{-1}\bold{y_0},
\end{align*}
where $z=h\lambda$.

Therefore,
\begin{align*}
\bold{y_1} = \bold{y_0}+z b^T(I-zA)^{-1}\bold{y_0}=(1+z b^T(I-zA)^{-1}e)\bold{y_0},
\end{align*}
where $e=(1,\dots,1)^T.$

Therefore in this case, the stability function is given by
\begin{align*}
R(z)=1+z b^T(I-zA)^{-1}e.
\end{align*}
Similarly if we take equations \eqref{col1} and \eqref{col2} and apply them to $y'=\lambda y$ taking $Y = (Y_{n,i})_{n,i = 1}^v$ we can rewrite \eqref{col1} and \eqref{col2} as
\begin{align*}
Y - z A Y =\bold{y_n},\\
-z b^T Y +y_{n+1} = y_n,
\end{align*}
which we can rewrite in matrix form as
\begin{align*}
\begin{pmatrix}
I-z A & 0\\
-z b^T & 1
\end{pmatrix}\begin{pmatrix}
Y \\ y_{n+1}
\end{pmatrix}= y_n\begin{pmatrix}
e \\1
\end{pmatrix}.
\end{align*}
Solving for $y_{n+1}$ using Crammer's rule we get
\begin{align*}
y_{n+1} = \frac{\det \begin{pmatrix}
I-z A & e\\
-z b^T & 1
\end{pmatrix}}{\det \begin{pmatrix}
I-z A & 0\\
-z b^T & 1
\end{pmatrix}}y_n.
\end{align*}
The numerator of the above equation is  equivalent to
\begin{align*}
\det \begin{pmatrix}
I-z A+z e b^T & 0\\
-z b^T & 1
\end{pmatrix}=\det (I-z(A-e b^T)).
\end{align*}
Similarly the denominator is equivalent to
\begin{align*}
\det (I- z A).
\end{align*}
Therefore 
\begin{align*}
y_{n+1}=\frac{\det (I-z(A-e b^T))}{\det (I- z A)} y_n,
\end{align*}
which implies that the stability function can also be written as
\begin{align}
R(z)=\frac{\det (I-z(A-e b^T))}{\det (I- z A)} = \frac{P(z)}{Q(z)}.\label{stability}
\end{align}
We note that the solution of $y_{n+1}$ given $y'=\lambda y$ is $\exp(z)$.

Therefore, if a Runge-Kutta collocation method is of order p, then
\begin{align*}
\exp(z) = R(z) + O(z^{p+1}).
\end{align*}
We can approximate $\exp(z)$ using Pad\'{e}'s approximation which is written as
\begin{align*}
\exp(z) = \frac{P_{lm}(z)}{Q_{lm}(z)},
\end{align*}
where
\begin{align*}
P_{lm}(z) &= \frac{l!}{(l+m)!}\sum_{i=0}^l\frac{(l+m-i)!}{i!(l-i)!}z^i,\\
Q_{lm}(z) &= \frac{m!}{(l+m)!}\sum_{i=0}^l\frac{(l+m-i)!}{i!(m-i)!}z^i.
\end{align*}
From this formulations and upon calculations, we find that though the Lobatto IIIA have the highest possible order as mentioned earlier, their stability function is such that $|R(\infty)|=1$ thus they are not good enough for solving stiff differential equations, that is, they are not {\em $L$-stable}.

