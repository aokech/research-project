\chapter{Numerical experiments}
To conduct the effectiveness of this method in solving stiff differential equations, we have selected well known problems that have appeared in the literature. We will now  put into practice what we have discussed so far.

\subsection{A prothero-Robinson equation}\citet{prothero}.
The Prothero Robinson equation is given by the following equation 
\begin{align}
\begin{cases}
y'(t)=\lambda(y(t)-g(t))+g'(t), \quad t\in [0,10],\\
y(0) = 0,
\end{cases}\label{prot}
\end{align}
where $\lambda=-10^6$, $g(t)=\sin(t)$, and its exact solution is given by $y(t)=\sin (t)$.

To demonstrate our system in solving this stiff equation, we plot our approximated results and compare it with the exact solution over the same range. We also make a table to compare the error we obtain at different step-sizes.

Note that in this problem we have used the case when $v=2$.

The plot is presented below

\begin{figure}[h!]
\centering
\fbox{
\includegraphics[scale=0.5]{prothero}
}
\caption{Exact and numerical solutions for the Prothero Robinson equation \eqref{prot}}
\label{figure1}
\end{figure}
\FloatBarrier

We present errors with different step-sizes in Table \eqref{protherotable}.
\begin{table}[ht]
\caption{Demonstration of dependence of error on step-sizes}
\centering
\begin{tabular}{|c | c c c c c|}
\hline
$t$ & $h=2$ & $h=1$ & $h=0.5$ & $h=0.25$ & $h=0.125$\\
\hline
$0$ & $0.000$  & $0.000$ & $0.00$ & $0.00$ & $0.000$\\
$2$&$1.31 E{-7}$ & $3.02 E{-8}$ & $7.42 E{-9}$ & $1.85 E{-9}$ & $4.60 E{-10}$\\
$4$ & $1.53E{-7}$ & $3.53E{-8}$ & $8.67 E{-9}$ & $2.16 E{-9}$ & $5.37E{-10}$\\
$6$ & $3.68E{-9}$ & $8.49E{-8}$ & $2.07 E{-10}$ & $5.04 E{-11}$ & $1.13E{-11}$\\
$8$ & $1.06E{-7}$ & $2.45E{-8}$ & $6.00 E{-9}$ & $1.49 E{-9}$ &$3.71E{-10}$\\
$10$ & $1.70E{-7}$ & $3.93E{-8}$ & $9.64 E{-9}$ & $2.40 E{-9}$ & $5.96E{-10}$\\
$12$ & $1.45E{-8}$ & $3.33E{-9}$ & $8.15 E{-10}$ & $2.00 E{-10}$ & $4.77E{-11}$\\
$14$ & $7.99E{-8}$ & $1.84E{-8}$ & $4.52 E{-9}$ & $1.12 E{-9}$ & $2.78E{-10}$\\
$16$ & $1.81E{-7}$ & $4.18E{-8}$ & $1.03 E{-8}$ & $2.55 E{-9}$ & $6.33E{-10}$\\
$18$ & $3.15E{-8}$ & $7.25E{-9}$ & $1.78 E{-9}$ & $4.38 E{-10}$ & $1.06E{-10}$\\
$20$ & $5.48E{-8}$ & $1.26E{-8}$ & $3.10 E{-9}$ & $7.67 E{-10}$ & $1.88E{-10}$\\
\hline
\end{tabular}
\label{protherotable}
\end{table}

From the above experiment we observe that our numerical approximation is close to the exact solution even for large step-sizes and therefore the approximated solution mimic the exact solution. Moreover, if we take small step-sizes then the approximation has minimal errors as indicated in the table above. Therefore we can say that methods with small step-sizes perform better than those with large step-sizes.

%We can therefore conclude that this collocation method will provide good numerical results for stiff problems of this kind.

Figure \eqref{figure1} shows that all the points we get as a result of the numerical approximation fits the exact solution accurately and therefore it is a good approximate to problems of this type.

From table \eqref{protherotable}, it can be seen that the order of convergence is 2. 

From table \eqref{protherotable}, we note that as the value of $t$ increases, the error remain fairly constant for the different values of $h$.

\subsection{A mildly stiff linear System}\citet{jesus}.
In this problem we consider the following system of differential equations
\begin{align}
\begin{dcases}
y'_1(t)=998 y_1(t)+1998y_2(t),\\
y_2'(t)=-999 y_1(t)-1999y_2(t),
\end{dcases}\label{mildst}
\end{align}
with initial values $y_1(0)=1$ and $y_2(0)=1$. Its exact solution is given by 
\begin{align*}
\begin{cases}
y_1(t)=4 e^{-t}-3 e^{-1000 t},\\
y_2(t)=-2 e^{-t}+3 e^{-1000 t}.
\end{cases}
\end{align*}

The stiffness ratio of this problem is $1:1000$. We find the numerical solution for the values of $t$ between $[0,0.04]$ with our initial $h_0=0.001$.  As done in the previous example, we will present the errors in a table and the plot of the approximated solution and its exact solution over the same interval. There will be two plots, the first one for $y_1(t)$ and the other one for $y_2(t)$.

\begin{figure}[h!]
\centering
\fbox{
\includegraphics[scale=0.4]{secondmild}
}
\caption{Exact and numerical solutions for $y_1(t)$ of the mild stiff problem \eqref{mildst}}
\end{figure}
\FloatBarrier
\begin{figure}[h!]
\centering
\fbox{
\includegraphics[scale=0.4]{firstmild}
}
\caption{Exact and numerical solutions for $y_2(t)$ of the mild stiff problem \eqref{mildst}}
\end{figure}
\FloatBarrier

We present errors obtained when we use variants of an RKC method by using $v=2$, $v=4$ and $v=6$ and compare the three instances.

\begin{table}[ht]
\caption{Errors for $y_1$ using the classical Runge-Kutta method and the collocation method at different steps when $v=2$, $v=4$, $v=6$ and $v=8$.}
\centering
\begin{tabular}{|c|c c c c c|}
\hline
$t$ &Classical RK & RKC ($v=2$) & RKC ($v=4$)& RKC ($v=6$) & RKC ($v=8$)\\
\hline
$0$ & $0.00$ & $0.00$ & $0.00$ & $0.00$ & $0.00$\\
$0.005$ & $0.78$ & $1.49E-4$ & $1.26E-7$ & $3.42E-11$ & $4.44 E{-15}$\\
$0.010$ & $0.77$ & $2.02E-6$ & $1.70E-9$ & $4.83E-13$ & $1.95 E{-14}$\\
$0.015$& $0.75$ & $2.05E-8$& $1.72E-11$ & $4.31E-14$ & $5.33 E{-14}$\\
$0.020$& $0.73$&  $1.85E-10$ & $1.98E-13$ & $5.95 E{-14}$ & $8.17 E{-14}$\\
$0.025$ & $0.72$ & $1.51E-12$ & $5.60E-14$ & $8.53E-14$ & $1.21 E{-13}$\\
$0.030$ & $0.700$ & $6.26E-14$ & $7.64E-14$ & $1.04E-13$ & $1.41 E{-13}$\\
$0.035$ & $0.68$ & $9.99E-14$ & $9.55E-14$ & $1.11E-13$ & $1.65 E{-13}$\\
$0.040$ & $0.67$ & $8.04E-14$ & $1.30E-13$ & $1.22E-13$ & $1.64 E{-13}$\\
\hline
\end{tabular}
\end{table}

Similarly for $y_2$ we present the errors obtained is presented in the table below

\begin{table}[h!]
\caption{Errors for $y_2$ using the classical Runge-Kutta method in the first column and the collocation method at different steps when $v=2$, $v=4$, $v=6$ and $v=8$.}
\centering
\begin{tabular}{| c | c c c c c |}
\hline 
$t$ & Classical RK & RKC ($v=2$) & RKC ($v=4$)& RKC ($v=6$) & RKC ($v=8$)\\
\hline
$0$ & $0.00$ & $0.00$ & $0.00$ & $0.00$ & $0.00$\\
$0.005$ & $0.39$ & $1.5E-4$ & $1.26E-7$& $3.42E-11$ & $9.33 E{-15}$\\
$0.010$ & $0.38$ & $2.02E-6$ & $6.87E-10$ & $4.73E-13$ & $5.11 E{-15}$\\
$0.015$ & $0.38$ & $2.05E-8$ & $1.72E-11$ & $2.26E-14$ & $2.46 E{-14}$\\
$0.020$ & $0.37$ & $1.85E-10$ & $1.75E-13$ & $2.84E-14$ & $3.55 E{-14}$\\
$0.025$& $0.35$ & $5.69E-13$ & $2.84E-14$ & $4.22E-14$ & $5.35 E{-14}$ \\
$0.030$ & $0.35$ & $2.24E-14$ & $3.80E-14$ & $5.20E-14$ & $6.51 E{-14}$\\
$0.035$ & $0.34$ & $4.82E-14$ & $4.60E-14$ & $5.60E-14$ & $7.93 E{-14}$\\
$0.040$ & $0.33$ & $3.97E-14$ & $6.24E-14$ & $6.11E-14$ & $8.22 E{-14}$\\
\hline
\end{tabular}
\end{table}
\FloatBarrier

One thing to note is that the cost of doing the experiment increases as the value of $v$ increases due to the time it takes to generate the matrix $A$. However, even $v=2$ gives us pretty good approximation at certain points.

%as $v$ increases and the cost of doing the vast simultaneous equations at large values of $v$.

But even though the cost increases, it is worth noting as demonstrated in the table above that the accuracy of the numerical approximation (especially for the first iterations) increases as the value of $v$ increases.

The numerical approximation of the stiff problem with the classical Runge-Kutta method is not as accurate as the collocation method that we have chosen and this may be due to the fact that they are  not $A$-stable.

The plots provided for this problem shows that the numerical solution coincides with the exact solution and we can therefore draw a conclusion that the collocation method that we chose is suitable to solving this type of stiff problems.

%When $v=2$ the numerical approximation above start with order $4$ and after several iteration, the order switches to the highest possible order in SAGE which is order 14. Similarly for the case when $v=4$ and $v=6$ we start with order $7$ and $11$ respectively after which the order switches to order $13$ and $14$. And therefore, after a short initial phase, the code to computing the approximation of the stiff problems will switch to the highest possible order.

